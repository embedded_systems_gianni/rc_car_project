#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <stdbool.h>
#include <sys/time.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>

/* Global variables */
unsigned char buffer[1];
struct sockaddr_in serverAddr, clientAddr;
int udpSocket, slen = sizeof(clientAddr);

bool fullForwardPressed = false;
bool forwardPressed = false;
bool backwardPressed = false;
bool rightPressed = false;
bool leftPressed = false;
bool emergencyPressed = false;
/* Function Declaration */
void ConnectionSocket(char* ip);
void control_car(unsigned char *buffer);
void initialize_pins();
void full_forward();
void forward();
void backward();
void left();
void right();
void release_steer();
void release_engine();
void emergency_stop();
void handle_pins();
void loop();
int hostname_to_ip(char *  , char *);

int main(){
    setbuf(stdout, NULL); // Disable buffering of stdout

    char *hostname = "BANANAPI.local";
    char ip[100];

    hostname_to_ip(hostname , ip);
    printf("%s resolved to %s" , hostname , ip);

    initialize_pins();
    ConnectionSocket(ip);
    loop();

    return 0;
}

int hostname_to_ip(char * hostname , char* ip)
{
    // hostname to ip
    struct hostent *he;
    struct in_addr **addr_list;
    int i;

    if ( (he = gethostbyname( hostname ) ) == NULL)
    {
        // get the host info
        herror("gethostbyname");
        return 1;
    }

    addr_list = (struct in_addr **) he->h_addr_list;

    for(i = 0; addr_list[i] != NULL; i++)
    {
        //Return the first one;
        strcpy(ip , inet_ntoa(*addr_list[i]) );
        return 0;
    }

    return 1;
}

void ConnectionSocket(char* ip){

    /*Create UDP socket*/
    udpSocket = socket(AF_INET, SOCK_DGRAM, 0);
	
	memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

    /*Configure settings in address struct*/
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(7891);
    serverAddr.sin_addr.s_addr = inet_addr(ip);
	
    bind(udpSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));

}

void loop(){
    while(1){
    /* Try to receive any incoming UDP datagram. */
    int recvlen = recvfrom(udpSocket,buffer,1,0,(struct sockaddr *)&clientAddr, &slen); // Blocking!
		if (recvlen >= 0) {
			printf("Received packet from %s:%d\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));
			control_car(buffer);
			handle_pins();
		}
		else{
			printf("Error occured!");
		}
  }
}

void control_car(unsigned char *buffer){
    printf("Button: %x\n", buffer[0]);

    /* FULL FORWARD */

    if(buffer[0] == 0x1){
        fullForwardPressed = true;
    }
    else if(buffer[0] == 0x2){
        fullForwardPressed = false;
    }

    /* FORWARD */

    if(buffer[0] == 0x3){
        forwardPressed = true;
    }
    else if(buffer[0] == 0x4){
        forwardPressed = false;
    }

    /* BACKWARD */

    if(buffer[0] == 0x5){
        backwardPressed = true;
    }
    else if(buffer[0] == 0x6){
        backwardPressed = false;
    }

    /* LEFT */

    if(buffer[0] == 0x9){
        leftPressed = true;
    }
    else if(buffer[0] == 0x10){
        leftPressed = false;
    }

    /* RIGHT */

    if(buffer[0] == 0x7){
        rightPressed = true;
    }
    else if(buffer[0] == 0x8){
        rightPressed = false;
    }

    /* EMERGENCY */
    if(buffer[0] == 0x11){
        emergencyPressed = true;
    }
    else {
        emergencyPressed = false;
    }
}

void handle_pins(){

    /* ENGINE */

    if(fullForwardPressed == true){
        full_forward();
    }
    else if(forwardPressed == true){
        forward();
    }
    else if(backwardPressed == true){
        backward();
    }
    else{
        release_engine();
    }

    /* STEER */

    if(leftPressed == true){
        left();
    }
    else if(rightPressed == true){
        right();
    }
    else{
        release_steer();
    }

    /* EMERGENCY */

    if(emergencyPressed == true){
        emergency_stop();
    }
}

void initialize_pins(){
    /*(Pin, '0' = IN '1' = OUT, '2' = PWM_OUT) */

    //USE BCM PIN LAYOUT
    wiringPiSetupGpio();

    //PWM PINS
    pinMode(18, 2); // PWM CHANNEL 0
    pinMode(13, 2); // PWM CHANNEL 1

    // GPIO PINS
    pinMode(23, 1);
    pinMode(24, 1);
    pinMode(17, 1);
    pinMode(27, 1);

    // SET PWM MODE
    pwmSetMode(PWM_MODE_MS);
}

void full_forward(){
    /* PWMFreq = 19.2e6 Hz / pwmClock / pwmRange */
    // 10V, DUTY CYCLE 100% EQUALS 10V
    pwmSetClock(1920);
    pwmSetRange(200);
    pwmWrite(18, 200);

    // PIN 23 = 1, PIN 24 = 0, MOTOR TURNS RIGHT
    digitalWrite(23, HIGH);
    digitalWrite(24, LOW);
}

void forward(){
    //PWMFreq = 19.2e6 Hz / pwmClock / pwmRange
    // 10V, DUTY CYCLE 50% EQUALS 5V
    pwmSetClock(1920);
    pwmSetRange(200);
    pwmWrite(18, 100);

    // PIN 23 = 1, PIN 24 = 0, MOTOR TURNS RIGHT
    digitalWrite(23, HIGH);
    digitalWrite(24, LOW);
}

void backward(){
    // PWMFreq = 19.2e6 Hz / pwmClock / pwmRange
    // 10V, DUTY CYCLE 50% EQUALS 5V
    pwmSetClock(1920);
    pwmSetRange(200);
    pwmWrite(18, 100);

    // PIN 23 = 0, PIN 24 = 1, MOTOR TURNS LEFT
    digitalWrite(23, LOW);
    digitalWrite(24, HIGH);
}

void left(){
    //PWMFreq = 19.2e6 Hz / pwmClock / pwmRange
    // 10V, DUTY CYCLE 100% EQUALS 10V
    pwmSetClock(1920);
    pwmSetRange(200);
    pwmWrite(13, 200);

    // PIN 23 = 0, PIN 24 = 1, MOTOR TURNS LEFT
    digitalWrite(17, HIGH);
    digitalWrite(27, LOW);
}

void right(){
    // PWMFreq = 19.2e6 Hz / pwmClock / pwmRange
    // 10V, DUTY CYCLE 100% EQUALS 10V
    pwmSetClock(1920);
    pwmSetRange(200);
    pwmWrite(13, 200);

    // PIN 23 = 1, PIN 24 = 0, MOTOR TURNS RIGHT
    digitalWrite(17, LOW);
    digitalWrite(27, HIGH);
}

void release_steer(){
    // RELEASE STEER MOTOR
    digitalWrite(17, LOW);
    digitalWrite(27, LOW);
}

void release_engine(){
    // RELEASE ENGINE
    digitalWrite(23, LOW);
    digitalWrite(24, LOW);
}

void emergency_stop(){
    // RELEASE ALL MOTORS
    pwmWrite(13, 0);
    pwmWrite(18, 0);
    digitalWrite(17, LOW);
    digitalWrite(27, LOW);
    digitalWrite(23, LOW);
    digitalWrite(24, LOW);
}