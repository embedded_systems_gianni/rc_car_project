package com.example.gianni.rccar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Objects;


public class MainActivity extends AppCompatActivity {
    private static final String REGEX_IPADDRES = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    ImageButton Forward;
    ImageButton Backward;
    ImageButton Left;
    ImageButton Right;
    EditText Ip;
    Button Set;
    WebView Cam;

    public int port = 7891;
    public Socket socket;
    final byte[][] data = new byte[1][1];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageButton fullFoward = (ImageButton) findViewById(R.id.buttonFullFoward);
        Forward = (ImageButton) findViewById(R.id.buttonForward);
        Backward = (ImageButton) findViewById(R.id.buttonBackward);
        Left = (ImageButton) findViewById(R.id.buttonLeft);
        Right = (ImageButton) findViewById(R.id.buttonRight);
        Ip = (EditText) findViewById(R.id.editTextIp);
        Set = (Button) findViewById(R.id.buttonSet);
        Cam = (WebView) findViewById(R.id.Camera);

        fullFoward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        data[0] = new byte[]{0x1};
                        sendData(data[0]);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        data[0] = new byte[]{0x2};
                        sendData(data[0]);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        Forward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        data[0] = new byte[]{0x3};
                        sendData(data[0]);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        data[0] = new byte[]{0x4};
                        sendData(data[0]);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        Backward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        data[0] = new byte[]{0x5};
                        sendData(data[0]);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        data[0] = new byte[]{0x6};
                        sendData(data[0]);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        Right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        data[0] = new byte[]{0x7};
                        sendData(data[0]);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        data[0] = new byte[]{0x8};
                        sendData(data[0]);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        Left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        data[0] = new byte[]{0x9};
                        sendData(data[0]);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        data[0] = new byte[]{0x10};
                        sendData(data[0]);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        Set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Ip.getText().toString().matches(REGEX_IPADDRES)){
                    new Thread(new myTask(Ip.getText().toString())).start();
                    setCamera(Ip.getText().toString());
                }

            }
        });

        Cam.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                String message = "Camera Connection failed, is the server online? Please reload.";
                Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
                toast.show();
                Cam.loadUrl("about:blank");
                super.onReceivedError(view, request, error);
            }
        });

        VideoSettings();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.reload) {
            String message = "Reloading..";
            Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
            toast.show();
            new Thread(new myTask(Ip.getText().toString())).start();
            setCamera(Ip.getText().toString());
        }
        return super.onOptionsItemSelected(item);
    }

    public void sendData(byte[] data){
        new Thread(new myTask2(data)).start();
    }

    public void VideoSettings() {
        Cam.getSettings().setLoadWithOverviewMode(true);
        Cam.getSettings().setUseWideViewPort(true);
    }

    public void setCamera(String address){
        Cam.loadUrl("http://" + address + ":9090/stream/video.mjpeg");
    }

    private class myTask implements Runnable {
        String address;

        myTask(String address) {
            this.address = address;
        }

        @Override
        public void run() {
            try {
                socket = new Socket(this.address, port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class myTask2 implements Runnable {
        byte[] data;

        myTask2(byte[] bytes) {
            this.data = bytes;
        }

        @Override
        public void run() {
            try {
                if (socket != null){
                    OutputStream out = socket.getOutputStream();
                    out.write(this.data);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
