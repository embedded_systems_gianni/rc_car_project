package com.example.gianni.rccar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class MainActivity extends AppCompatActivity {

    ImageButton FullFoward;
    ImageButton Forward;
    ImageButton Backward;
    ImageButton Left;
    ImageButton Right;
    EditText Ip;
    Button Set;
    WebView Cam;
    final byte[][] data = new byte[1][1];
    String address = "0.0.0.0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FullFoward = (ImageButton) findViewById(R.id.buttonFullFoward);
        Forward = (ImageButton) findViewById(R.id.buttonForward);
        Backward = (ImageButton) findViewById(R.id.buttonBackward);
        Left = (ImageButton) findViewById(R.id.buttonLeft);
        Right = (ImageButton) findViewById(R.id.buttonRight);
        Set = (Button) findViewById(R.id.buttonSet);
        Ip = (EditText) findViewById(R.id.editTextIp);
        Cam = (WebView) findViewById(R.id.Camera);

        FullFoward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        data[0] = new byte[]{0x1};
                        ServerSocket(data[0]);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        data[0] = new byte[]{0x2};
                        ServerSocket(data[0]);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        Forward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        data[0] = new byte[]{0x3};
                        ServerSocket(data[0]);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        data[0] = new byte[]{0x4};
                        ServerSocket(data[0]);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        Backward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        data[0] = new byte[]{0x5};
                        ServerSocket(data[0]);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        data[0] = new byte[]{0x6};
                        ServerSocket(data[0]);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        Right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        data[0] = new byte[]{0x7};
                        ServerSocket(data[0]);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        data[0] = new byte[]{0x8};
                        ServerSocket(data[0]);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        Left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        data[0] = new byte[]{0x9};
                        ServerSocket(data[0]);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        data[0] = new byte[]{0x10};
                        ServerSocket(data[0]);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        Set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                address = Ip.getText().toString();
                Cam.loadUrl("http://" + address + ":9090/stream/video.mjpeg");

            }
        });



        Cam.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                String message = "Camera Connection failed, is the server online? Please reload.";
                Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
                toast.show();
                Cam.loadUrl("about:blank");
                super.onReceivedError(view, request, error);
            }
        });

        VideoSettings();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.reload) {
            String message = "Reloading..";
            Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
            toast.show();
            Cam.loadUrl("http://" + address + ":9090/stream/video.mjpeg");
        }
        if (id == R.id.emergency) {
            data[0] = new byte[]{0x11};
            ServerSocket(data[0]);
        }

        return super.onOptionsItemSelected(item);
    }

    void ServerSocket(byte[] data){
        DatagramSocket socket = null;

        try {
            // Connection info

            InetAddress host = InetAddress.getByName(address);
            int port = 7891;

            // Construct the socket

            socket = new DatagramSocket();

            // Construct the datagram packet
            DatagramPacket packet = new DatagramPacket(data, data.length,
                    host, port);

            // Send it
            socket.send(packet);

        }catch (Exception e){
            Log.e("ClientException", e.getMessage(), e);

        }finally {
            if( socket != null)
                socket.close();
        }

    }

    void VideoSettings() {
        Cam.getSettings().setLoadWithOverviewMode(true);
        Cam.getSettings().setUseWideViewPort(true);
    }
}
