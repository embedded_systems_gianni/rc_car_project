import socket
import sys
import time
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PWMControl_ui import Ui_Form
import cv2

""" INFORMATION: FF = b'\x01', FW = b'\x03', BW = b'\x05', RI = b'\x07', LE = b'\x09', EM = b'\x11'
UNEVEN NUMBERS ARE FOR PRESSED CONDITIONS """

""" INFORMATION: FF = b'\x02', FW = b'\x04', BW = b'\x06', RI = b'\x08', LE = b'\x10'
EVEN NUMBERS ARE FOR RELEASED CONDITIONS """


class MainWindow(QWidget, Ui_Form):
    def __init__(self, UDP_IP, UDP_PORT):
        super(MainWindow, self).__init__()
        """ UDP related """
        self.UDP_IP = UDP_IP
        self.UDP_PORT = UDP_PORT
        self.sock = None

        """ Ui related """
        self.setupUi(self)
        self.setWindowTitle("RC Car Controller")
        self.setWindowIcon(QIcon("6269-200.png"))
        self.thread = VideoThread(self.UDP_IP)

        """ Initialize the socket """
        self.initialize_socket()

        """ Thread for camera """
        self.thread.video.connect(self.setFrame)
        self.thread.start()

    def initialize_socket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP

    @pyqtSlot(name="")
    def on_btnFullForward_pressed(self):
        self.sock.sendto(b'\x01', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(name="")
    def on_btnFullForward_released(self):
        self.sock.sendto(b'\x02', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(name="")
    def on_btnForward_pressed(self):
        self.sock.sendto(b'\x03', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(name="")
    def on_btnForward_released(self):
        self.sock.sendto(b'\x04', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(name="")
    def on_btnBackward_pressed(self):
        self.sock.sendto(b'\x05', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(name="")
    def on_btnBackward_released(self):
        self.sock.sendto(b'\x06', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(name="")
    def on_btnLeft_pressed(self):
        self.sock.sendto(b'\x09', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(name="")
    def on_btnLeft_released(self):
        self.sock.sendto(b'\x10', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(name="")
    def on_btnRight_pressed(self):
        self.sock.sendto(b'\x07', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(name="")
    def on_btnRight_released(self):
        self.sock.sendto(b'\x08', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(name="")
    def on_btnEmergency_pressed(self):
        self.sock.sendto(b'\x11', (self.UDP_IP, self.UDP_PORT))

    @pyqtSlot(object, name="")
    def setFrame(self, frame):
        pixmap = QPixmap.fromImage(frame)
        self.picFeed.setPixmap(pixmap)

    def close_application(self):
        time.sleep(1)
        self.setWindowTitle("Closing application")
        sys.exit()

    def closeEvent(self, event):
        event.ignore()
        self.close_application()


class VideoThread(QThread):
    video = pyqtSignal(object)

    def __init__(self, UDP_IP):
        super(VideoThread, self).__init__()
        self.UDP_IP = UDP_IP

    def run(self):
        try:
            cap = cv2.VideoCapture("http://" + self.UDP_IP + ":9090/stream/video.mjpeg")
        except Exception as e:
            print("Lost Connection" + "  " + str(e))
        while cap.isOpened():
            ret, frame = cap.read()
            newFrame = cv2.flip(frame, 0)
            # adjust width en height to the preferred values
            image = QImage(newFrame.tostring(), 640, 480, QImage.Format_RGB888).rgbSwapped()
            self.video.emit(image)

if __name__ == '__main__':
    addr = socket.gethostbyname("BANANAPI.local")
    UDP_IP = addr
    UDP_PORT = 7891

    app = QApplication([])
    window = MainWindow(UDP_IP, UDP_PORT)
    window.show()
    sys.exit(app.exec_())


